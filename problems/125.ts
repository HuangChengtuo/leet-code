function isPalindrome (s: string): boolean {
  const arr = s.split('')
  let left = 0
  let right = arr.length - 1
  while (left < right) {
    if (!/^[a-zA-Z0-9]$/.test(arr[left])) {
      left++
    } else if (!/^[a-zA-Z0-9]$/.test(arr[right])) {
      right--
    } else if (arr[left].toLowerCase() !== arr[right].toLowerCase()) {
      return false
    } else {
      left++
      right--
    }
  }
  return true
};

export default function() {
  return isPalindrome(' ')
}
