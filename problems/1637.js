/**
 * @param {number[][]} points
 * @return {number}
 */
const maxWidthOfVerticalArea = function (points) {
  const arr = []
  let result = 0
  for (const point of points) {
    arr.push(point[0])
  }
  arr.sort((a, b) => a - b)
  for (let i = 1; i < arr.length; i++) {
    result = Math.max(result, arr[i] - arr[i - 1])
  }
  return result
};

module.exports = function () {
  const points = [[3, 1], [9, 0], [1, 0], [1, 4], [5, 3], [8, 8]]
  console.log(maxWidthOfVerticalArea(points))
}