class NumArray {
  arr: number[]
  constructor (nums: number[]) {
    this.arr = nums
  }

  sumRange (left: number, right: number): number {
    let result = 0
    for (let i = left; i <= right; i++) {
      result += this.arr[i]
    }
    return result
  }
}

export default function () {
  const arr = new NumArray([-2, 0, 3, -5, 2, -1])
  return arr.sumRange(0, 5)
}