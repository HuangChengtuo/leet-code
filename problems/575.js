/**
 * @param {number[]} candyType
 * @return {number}
 */
const distributeCandies = function (candyType) {
  const set = new Set(candyType)
  return Math.min(set.size, candyType.length / 2)
};

module.exports = function () {
  return distributeCandies([6, 6, 6, 6])
}