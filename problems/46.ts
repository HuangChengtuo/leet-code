function permute(nums: number[]): number[][] {
  const result: number[][] = []
  function fn(combo: number[], rest: number[]) {
    if (!rest.length) {
      result.push(combo)
      return
    }
    for (const num of rest) {
      fn([...combo, num], rest.filter(item => item !== num))
    }
  }
  for (const num of nums) {
    fn([num], nums.filter(item => item !== num))
  }
  return result
};

export default function() {
  console.log(permute([1, 1, 2]))
}