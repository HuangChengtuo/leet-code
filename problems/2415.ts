import { TreeNode } from '../DataStructure'

function reverseOddLevels (root: TreeNode | null): TreeNode | null {
  function fn (node: TreeNode, depth: number) {
    if (!node.left) {
      return root
    }
    if (!(depth % 2)) {
      const temp = node.left.val
      node.left.val = node.right.val
      node.right.val = temp
    }
    fn(node.left, depth + 1)
    fn(node.right, depth + 1)
  }
  fn(root, 0)
  return root
};

export default function() {
  const tree = new TreeNode(
    0,
    new TreeNode(1, new TreeNode(0), new TreeNode(0)),
    new TreeNode(2, new TreeNode(0), new TreeNode(0))
  )
  console.log(reverseOddLevels(tree))
}