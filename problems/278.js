/**
 * Definition for isBadVersion()
 *
 * @param {integer} version number
 * @return {boolean} whether the version is bad
 * isBadVersion = function(version) {
 *     ...
 * };
 */

/**
 * @param {function} isBadVersion()
 * @return {function}
 */
var solution = function (isBadVersion) {
  /**
   * @param {integer} n Total versions
   * @return {integer} The first bad version
   */
  return function (n) {
    let left = 1
    let right = n
    console.log(left, right)
    while (left < right - 1) {
      const middle = Math.ceil((right + left) / 2)
      if (isBadVersion(middle)) {
        right = middle
      } else {
        left = middle
      }
    }
    return isBadVersion(left) ? left : right
  };
};

module.exports = function () {
  const fn = solution((n) => n >= 1);
  console.log(fn(2))
}
