import { TreeNode } from '../DataStructure'

function binaryTreePaths (root: TreeNode | null): string[] {
  const result: string[] = []
  function fn (node: TreeNode, routes: number[]) {
    const newRoutes = [...routes, node.val]
    if (!node.left && !node.right) {
      result.push(newRoutes.join('->'))
    } else {
      node.left && fn(node.left, newRoutes)
      node.right && fn(node.right, newRoutes)
    }
  }
  fn(root, [])
  return result
};

export default function () {
  const tree = new TreeNode(1)
  return binaryTreePaths(tree)
}