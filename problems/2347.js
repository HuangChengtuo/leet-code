/**
 * @param {number[]} ranks
 * @param {string[]} suits
 * @return {string}
 */
const bestHand = function (ranks, suits) {
  const suitsSet = new Set();
  for (const suit of suits) {
    suitsSet.add(suit);
  }
  if (suitsSet.size === 1) {
    return "Flush";
  }
  const h = new Map();
  for (const rank of ranks) {
    h.set(rank, (h.get(rank) || 0) + 1);
  }
  if (h.size === 5) {
    return "High Card";
  }
  for (const value of h.values()) {
    if (value > 2) {
      return "Three of a Kind";
    }
  }
  return "Pair";
};

module.exports = function () {
  const ranks = [13, 2, 3, 1, 9]
  const suits = ["a", "a", "a", "a", "a"]
  return bestHand(ranks, suits)
}