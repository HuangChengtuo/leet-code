function nextGreaterElement (nums1: number[], nums2: number[]): number[] {
  const stack: number[] = []
  const map: Map<number, number> = new Map()
  for (let i = nums2.length - 1; i >= 0; i--) {
    while (stack.length && stack[stack.length - 1] < nums2[i]) {
      stack.pop()
    }
    map.set(nums2[i], stack.length ? stack[stack.length - 1] : -1)
    stack.push(nums2[i])
  }
  const result: number[] = []
  for (const key of nums1) {
    result.push(map.get(key))
  }
  return result
};

export default function() {
  return nextGreaterElement([4, 1, 2], [1, 3, 4, 2])
}